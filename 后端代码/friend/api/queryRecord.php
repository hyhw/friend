<?php
  include_once "../class/record.php";
  include_once "../query.php";
  include_once "../../src/requst/request_err.php";
  include_once "../../src/requst/request_succ.php";
  include_once "../../src/requst/param_empty.php";

  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods:*');
  header("Access-Control-Allow-Origin:*");

  $uid = $_POST['uid'];
  $to = $_POST['to'];

  $sql = 'SELECT * from record where (from_uid='.$uid.' and to_uid='.$to.') or (from_uid='.$to.' and to_uid='.$uid.') order by `create_time`';

  $result = queryEquip($sql);

  if($result->num_rows > 0) {
    $n = array();
    while ( $row = $result->fetch_assoc() ) {
      $record = new Record(
        $row['from_uid'],
        $row['to_uid'],
        $row['create_time'],
        $row['msg'],
        $row['rid']
      );

      array_push($n, $record);
    }
  } else {
    $re = $sql;

    echo json_encode($re);
  }

  $re = new ReSucc();
  $re->data = new stdClass();

  $re->data->record = $n;
  $re->totle = count($n) - 1;
  $re->sql = $sql;

  echo json_encode($re);
?>