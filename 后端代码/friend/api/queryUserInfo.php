<?php
  include_once "../class/user.php";
  include_once "../query.php";
  include_once "../../src/requst/request_err.php";
  include_once "../../src/requst/request_succ.php";
  include_once "../../src/requst/param_empty.php";

  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods:*');
  header("Access-Control-Allow-Origin:*");

  $uids = $_POST['uids'];

  $uidList = explode(',', $uids);

  $queryInfo = "SELECT * from `user` where uid in (".$uids.")";
  $InfoResult = queryEquip($queryInfo);

  if ( $InfoResult->num_rows > 0 ){
    $n = array();
    while ( $row = $InfoResult->fetch_assoc() ){
      $user = new user(
        $row["name"],
        $row["uid"],
        $row["pass"],
        $row["vx"],
        $row["friends"],
        $row["img"],
        $row["sex"],
        $row["age"],
        $row["email"],
        $row["address"],
        $row["interest"],
        $row["introduction"]
      );

      array_push( $n, $user );
    }

    $re = new stdClass();
    $re->infoList = $n;
    $re->sql = $queryInfo;
    echo json_encode($re);
  } else {
    $re = new stdClass();
    $re->sql = $queryInfo;
    $re->uids = $uids;
    $re->uidList = $uidList;

    echo json_encode($re);
  }
    
?>