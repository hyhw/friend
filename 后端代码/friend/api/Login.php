<?php
    include_once "../class/user.php";
    include_once "../query.php";
    include_once "../../src/requst/request_err.php";
    include_once "../../src/requst/request_succ.php";
    include_once "../../src/requst/param_empty.php";

    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods:*');
    header("Access-Control-Allow-Origin:*");
    
    //----获取post数据
    $sql_name=$_GET["name"];
    $sql_pass=$_GET["pass"];
    
    if(!$sql_pass||!$sql_name){
        $re = new  ReEmpty();
        echo json_encode($re);
    }
    else{
        $re = new ReSucc(); #---返回的数据
        $re->data = new stdClass();
        $re->login = 3;
    
        $sql = "select * from user where name='".$sql_name."'";
    
        $result = queryEquip($sql);    #---执行查询函数
    
        if($result->num_rows>0){    #--解析查询结果
            //$n=array();
            $row=$result->fetch_assoc();
            
            $user=new user(
                $row["name"],
                $row["uid"],
                $row["pass"],
                $row["vx"],
                $row["friends"],
                $row["img"],
                $row["sex"],
                $row["age"],
                $row["email"],
                $row["address"],
                $row["interest"],
                $row["introduction"]
            );
            //$n[$i++]=$user;
            $re->data->info = $user;
            $re->login = 2;
            //验证密码是否正确
            if($user->login($sql_pass)) {
                $re->login = 1;
            }
            
        }
        echo json_encode($re);
    }

?>