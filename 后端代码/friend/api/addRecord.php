<?php
    include_once "../class/user.php";
    include_once "../query.php";
    include_once "../../src/ulit/hyh.php";
    include_once "../../src/requst/request_err.php";
    include_once "../../src/requst/request_succ.php";

    header("Content-Type:text/html;charset=utf-8");
    header('Access-Control-Allow-Methods:*');
    header("Access-Control-Allow-Origin:*");

    $from = $_POST['from'];
    $to = $_POST['to'];
    $msg = $_POST['msg'];

    #----设置时区
    date_default_timezone_set("PRC");

    $createTime = date("Y-m-d h:i:s");
    $rid = time();

    $re = new ReError();
    if(!$re->reErr([$from, $to, $msg])) return;
    #--echo(json_encode([$sql_name, $sql_pass, $sql_phone, $sql_role_id, $sql_img]));

    $re = new ReSucc(); #---返回的数据
    $re->data = new stdClass();

    $sql = "INSERT INTO record VALUES (".$rid.", "
    .$from.", "
    .$to.", '"
    .$createTime."', '"
    .$msg."')";

    $result=queryEquip($sql);    #---执行查询函数

    if($result === true){
        
        $re->msg = "注册成功!";
        $re->data->state = 'succ';
        if($createTime) $re->data->createTime = $createTime;
        echo json_encode($re);
    }
    else{
        $re = new ReError();
        $re->msg = "数据库错误!";
        $re->data = new stdClass();
        $re->data->state = 'err';
        $re->data->errMsg = $result;
        echo json_encode($re);
    }
?>