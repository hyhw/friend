<?php
  include_once "../class/user.php";
  include_once "../query.php";
  include_once "../../src/requst/request_err.php";
  include_once "../../src/requst/request_succ.php";
  include_once "../../src/requst/param_empty.php";

  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods:*');
  header("Access-Control-Allow-Origin:*");

  $uid = $_GET['uid'];

  $sql = 'SELECT * from user where uid<>'.$uid.' order by `uid`';

  $result = queryEquip($sql);

  if($result->num_rows > 0) {
    $n = array();
    while ( $row = $result->fetch_assoc() ) {
      $user=new user(
        $row["name"],
        $row["uid"],
        $row["pass"],
        $row["vx"],
        $row["friends"],
        $row["img"],
        $row["sex"],
        $row["age"],
        $row["email"],
        $row["address"],
        $row["interest"],
        $row["introduction"]
      );

      array_push($n, $user);
    }
  } else {
    $re = $sql;

    echo json_encode($re);
  }

  $num = count($n) - 1;
  $user = $n[rand(0, $num)];

  $re = new ReSucc();
  $re->data = new stdClass();

  $re->data->user = $user;
  $re->sql = $sql;

  echo json_encode($re);
?>