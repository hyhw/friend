<?php

    class user{
        var $name;
        var $uid;
        var $pass;
        var $img;
        var $sex;
        var $email;
        var $address;
        var $interest;
        var $introduction;
        var $vx;
        var $age;
        var $friends;

        function __construct($name, $uid, $pass, $vx, $friends, $img, $sex, $age, $email, $address, $interest, $introduction){
            $this->name = $name;
            $this->uid = $uid;
            $this->pass = $pass;
            $this->vx = $vx;
            $this->friends = $friends;
            $this->img = $img;
            $this->sex = $sex;
            $this->age = $age;
            $this->email = $email;
            $this->address = $address;
            $this->interest = $interest;
            $this->introduction = $introduction;
        }

        public function login($pass){
            if($this->pass === $pass){
                return true;
            }
            return false;
        }
    }

?>