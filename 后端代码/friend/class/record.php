<?php
  class Record {
    var $from;
    var $to;
    var $createTime;
    var $msg;
    var $rid;

    function __construct($from, $to, $createTime, $msg, $rid) {
      $this->from = $from;
      $this->to = $to;
      $this->createTime = $createTime;
      $this->msg = $msg;
      $this->rid = $rid;
    }
  }
?>