<?php
    header('Content-Type: application/json');
    class ReError{
        var $msg = '出错了!';
        var $data;
        
        function reErr($arr){
            foreach($arr as $item){
                if(!$item) {
                    $this->put();
                    return false;
                }
                return true;
            }

            $this->data = new stdClass();
        }

        function put($msg = "参数错误！"){
            $this->msg = $msg;

            echo json_encode($this);
        }
    }
?>