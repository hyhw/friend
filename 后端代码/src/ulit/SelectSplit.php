<?php

    #-- 没有and 的简单拼接
    function SimplySplit( $ParamsObj ){
        if( $ParamsObj ) {
            $Str = "";
            foreach ( $ParamsObj as $key => $value ) {
                if ( $value ) {
                    $Str = $Str." ".$key."='".$value."' and";
                }
            }
            #  --剔除末尾的and
            $Str = replaceStr( $Str, 'and' );
            return $Str;
        }else{
            return false;
        }
    }

    function MySplit( $sql, $ParamsObj ){
        if ( $sql ){
            if( strstr($sql, 'and') ){
                $HasAnd = true;
                foreach ( $ParamsObj as $key => $value ) {
                    if ( $value ){
                        if($HasAnd) {
                            $sql = $sql." and";
                            $HasAnd = false;
                        }
                        $sql = $sql." ".$key."='".$value."'";
                    }
                    
                }
                return $sql;
            }else{
                return $sql.SimplySplit($ParamsObj);
            }
        }else{
            return SimplySplit($ParamsObj);
        }
    }

    function replaceStr( $arr, $str ){
        $index = strripos( $arr, $str );
        return substr_replace( $arr, '', $index );
    }
?>