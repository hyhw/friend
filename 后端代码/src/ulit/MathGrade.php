<?php
    class MathGrade{
        var $questions;
        var $answers;
        function __construct($questions, $answers){
            $this->questions = explode(",", strtolower( $questions));
            $this->answers = explode(",", strtolower( $answers));
        }

        public function check($questions, $answers){
            if($questions) $this->questions = explode(",", strtolower( $questions));
            if($answers) $this->answers = explode(",", strtolower( $answers));
            if(count($this->questions) == 1){
                if($this->questions === $this->answers){
                    return 5;
                }
                else return 0;
            }

            $true = [];
            $QuArr = $this->questions;
            $AnArr = $this->answers;
            for ($x = 0; $x <= count($QuArr)-1; $x++){
                if($QuArr[$x] === $AnArr[$x]) array_push($true, $QuArr[$x]);
            }
            return count($true)*5;
        }
    }
?>