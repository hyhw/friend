<?php
    class RandNum{
        #----排除掉重复数值
        var $NoNum;
        var $qidArr;
        var $min;
        var $max;

        function __construct($NoNum, $qidArr){
            $this->NoNum = $NoNum;
            $this->qidArr = $qidArr;
            $this->min = 0;
            $this->max = count($qidArr) - 1;
        }

        #---不可为NoNum中值
        public function GetRand(){
            if($this->max <= 0) return false;
            $randNum = $this->qidArr[rand($this->min, $this->max)];
            if(in_array($randNum, $this->NoNum)) return $this->GetRand();
            else return $randNum;
        }
    }
?>