import request from './request'

export function Login({name, pass}) {
  return request({
    url: '/friend/api/Login.php?name='+name+'&pass=' + pass,
    method: 'get',
    data: {
      name,
      pass
    }
  })
}

export function Logout(data) {
  return request({
    url: '/friend/api/Logon.php',
    method: 'get',
    data
  })
}

export function queryUser(param) {
  return request({
    url: '/friend/api/queryUser.php?uid='+param.uid,
    method: 'get',
    param
  })
}

export function queryUserInfo(data) {
  return request({
    url: '/friend/api/queryUserInfo.php',
    method: 'post',
    data
  })
}

// 添加聊天记录
export function addRecordApi(data) {
  return request({
    url: '/friend/api/addRecord.php',
    method: 'post',
    data
  })
}

// 查询聊天记录
export function queryAllRecord(data) {
  return request({
    url: '/friend/api/queryRecord.php',
    method: 'post',
    data
  })
}

// 注册
export function logon(data) {
  return request({
    url: '/friend/api/Logon.php',
    method: 'post',
    data
  })
}

// 修改用户信息
export function updateUserInfo(data) {
  return request({
    url: '/friend/api/updateUser.php',
    method: 'post',
    data
  })
}

// 添加好友
export function addFriendsApi(data) {
  return request({
    url: '/friend/api/addFriend.php',
    method: 'post',
    data
  })
}

