import axios from "axios";
import qs from "qs";

// 创建axios 实例

const request = axios.create({
  baseURL: "http://123.57.249.44:80",
  timeout: 6000, //请求超时时间
  transformRequest: [
    function (data, headers) {
      // 对 data 进行任意转换处理
      headers["Content-Type"] = "application/x-www-form-urlencoded"; //统一处理content-type ，通过qs工具将数据json格式转换为form data，注意不支持get类型
      return qs.stringify(data);
    },
  ],
});


// 错误处理函数
function errorHandler(err) {
  console.log(err);
}

// 请求拦截器
request.interceptors.request.use((config) => {
  //   const token = storage.get(ACCESS_TOKEN);
  // 如果 token 存在
  // 让每个请求携带自定义 token 请根据实际情况自行修改
  //   if (token) {
  //     config.headers["token"] = token;
  //   }
  return config;
}, errorHandler);

export default request;