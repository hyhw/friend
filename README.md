# 叫爸爸交友网站-毕业设计

#### 介绍
21年 毕业设计，使用vue2、vue-cli、php 、element-ui 开发
又到了交毕设的时间段了，受朋友委托，在3天内（上班摸鱼）开发出来的小项目。
时间比较紧张，所以bug比较多 :persevere: 希望各位不嫌弃吧。

网站：http://daokedao.xyz/object/obj/xin_friend

不知道为什么不太稳定，白屏的话换个浏览器吧。

#### 软件架构
前端： vue-cli
后端： PHP + Nginx


#### 安装教程

##### 安装依赖包
```
yarn install
```


#### 使用说明

##### 运行项目
```
yarn serve
```

##### 打包项目
```
yarn build
```

##### Lints and fixes files
```
yarn lint
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)